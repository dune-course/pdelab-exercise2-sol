#include<dune/common/fvector.hh>

#include<dune/pdelab/common/function.hh>

//! \brief Parameter class selecting boundary conditions
template<class BETA>
class BCTypeParam
  : public Dune::PDELab::DirichletConstraintsParameters
{
  const BETA &betaF;
public:

  BCTypeParam(const BETA &betaF_) : betaF(betaF_) { }

  //! Test whether boundary is Dirichlet-constrained
  template<typename I>
  bool isDirichlet(const I & intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1> & coord
                   ) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      xg = intersection.geometry().global( coord );

    if( xg[0]>1.0-1E-6 )
      return false; // no Dirichlet b.c. on the eastern boundary

    return true;  // Dirichlet b.c. on all other boundaries
  }

  //! Test whether boundary is Neumann
  template<typename I>
  bool isNeumann(const I & intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1> & coord
                 ) const
  {
    if(isDirichlet(intersection, coord))
      return false;

    typename BETA::Traits::RangeType beta;
    betaF.evaluate( intersection.inside(),
                    intersection.geometryInInside().global(coord),
                    beta );

    return intersection.outerNormal(coord) * beta <= 0;
  }

  //! Test whether boundary is outflow
  template<typename I>
  bool isOutflow(const I & intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1> & coord
                 ) const
  {
    if(isDirichlet(intersection, coord))
      return false;
    if(isNeumann(intersection, coord))
      return false;

    return true;
  }

};

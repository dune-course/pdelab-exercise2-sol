// -*- tab-width: 4; indent-tabs-mode: nil -*-
/** \file

    \brief Solve elliptic problem in constrained spaces with conforming finite elements
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include<iostream>
#include<map>
#include<string>
#include<vector>

#include<math.h>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/timer.hh>

#include<dune/grid/io/file/gmshreader.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/yaspgrid.hh>
#if HAVE_ALBERTA
#include<dune/grid/albertagrid.hh>
#endif
#ifdef HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>   // <--- This requires the new DUNE module "dune-alugrid".
#endif
#ifdef HAVE_ALUGRID
#include <dune/grid/alugrid.hh>   // <--- This requires the external library ALUGrid.
#endif
#if HAVE_UG
#include<dune/grid/uggrid.hh>
#endif

#include<dune/istl/bvector.hh>
#include<dune/istl/io.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/superlu.hh>

#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/finiteelementmap/qkfem.hh>
#include<dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/stationary/linearproblem.hh>

#include"example02_bctype.hh"
#include"example02_bcextension.hh"
#include"example02_operator.hh"

#include"example02c_P1.hh"

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
      {
        if(helper.rank()==0)
          std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
      }

    if (argc!=2)
      {
        if(helper.rank()==0)
          std::cout << "usage: ./example02c <meshfile>" << std::endl;
        return 1;
      }

    std::string meshfilename = argv[1];

    // sequential version
    if (1 && helper.size()==1)
    {

      typedef Dune::ALUGrid<2,2,Dune::simplex,Dune::conforming> GridType;

      Dune::shared_ptr<GridType> gridp(Dune::GmshReader<GridType>::read(meshfilename));

      // remove extension (if any) from meshfilename to construct VTK prefix
      std::size_t basestart = meshfilename.rfind('/');
      if(basestart != std::string::npos)
        ++basestart;
      else
        basestart = 0;
      std::size_t extstart = meshfilename.rfind('.');
      if(extstart == std::string::npos || extstart <= basestart)
        extstart = meshfilename.size();

      example02c_P1(gridp->leafView(),
                    "example02c_P1_"+meshfilename.substr(basestart, extstart));
    }
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    throw;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    throw;
  }
}

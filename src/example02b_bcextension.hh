/** \brief A function that defines Dirichlet boundary conditions AND
    its extension to the interior */
template<typename GV, typename RF, typename VEC>
class BCExtension_2b
  : public Dune::PDELab::GridFunctionBase<
           Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >,
           BCExtension_2b<GV,RF,VEC> >
{
  const GV& gv;
  const VEC& dx;

public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

  //! construct from grid view
  BCExtension_2b (const GV& gv_, const VEC& dx_)
    : gv(gv_), dx (dx_)
  { }

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    const int dim = Traits::GridViewType::Grid::dimension;
    typedef typename Traits::GridViewType::Grid::ctype ctype;
    Dune::FieldVector<ctype,dim> x = e.geometry().global(xlocal);

    if      (x[0]<1E-6 && x[1]>=0.25 && x[1]<=0.5)
      y = 1.0;
    // add regularization to the Dirichlet b.c.:
    else if (x[0]<1E-6 && ( x[1]>=0.25-dx[1] && x[1]<=0.5+dx[1] ) )
      y = 5.0 / 6.0;
    else if (x[0]<1E-6 && ( x[1]>=0.25-2.0*dx[1] && x[1]<=0.5+2.0*dx[1] ) )
      y = 1.0 / 6.0;
    else
      y = 0.0;

    return;
  }

  //! get a reference to the grid view
  inline const GV& getGridView () {return gv;}
};

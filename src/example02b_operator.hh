#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/pattern.hh>

// choose which method to use
enum Method { standardGalerkin, streamlineDiffusion };

/** a local operator for solving the convection diffusion equation
 *
 *   - \nabla \cdot \{ eps \nabla u + beta * u \} = f
 *                                               in \Omega
 *                                        u = g  on \Gamma_D
 *   \{ -eps \nabla u + u * beta \} \cdot n = j  on \Gamma_N = {}
 *               - eps \nabla u \cdot n = j_eps  on \Gamma_out = \partial\Omega\setminus\Gamma_D
 *
 * with conforming finite elements on all types of grids in any dimension
 *
 * \tparam BCType parameter class indicating the type of boundary condition
 */
template<class BCType, class BETA, class Eps>
class Example02bLocalOperator :
  public Dune::PDELab::NumericalJacobianApplyVolume
           <Example02bLocalOperator<BCType,BETA,Eps> >,
  public Dune::PDELab::NumericalJacobianVolume
           <Example02bLocalOperator<BCType,BETA,Eps> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary
           <Example02bLocalOperator<BCType,BETA,Eps> >,
  public Dune::PDELab::NumericalJacobianBoundary
           <Example02bLocalOperator<BCType,BETA,Eps> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary = true };                                // assemble boundary

  Example02bLocalOperator (const BCType& bctype_,  // boundary cond. type
                           const BETA& betaF_,     // convection field
                           Eps epsilon_,           // diffusion coefficient
                           Method method_,         // SG vs. SD
                           unsigned int intorder_=2) :
    bctype(bctype_), betaF(betaF_), epsilon(epsilon_), method(method_),
    intorder(intorder_)
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    // pull in some standard functions
    using std::pow;
    using std::abs;

    // assume Galerkin: lfsu == lfsv
    // This yields more efficient code since the local functionspace only
    // needs to be evaluated once, but would be incorrect for a finite volume
    // method

    // dimensions
    const int dim = EG::Geometry::dimension;
    const int dimw = EG::Geometry::dimensionworld;

    // extract some types
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType Jacobian;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType Range;
    typedef Dune::FieldVector<RF,dimw> Gradient;
    typedef typename LFSU::Traits::SizeType size_type;

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const Dune::QuadratureRule<DF,dim>&
      rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    for (const auto& ip : rule)
      {
        // evaluate basis functions on reference element
        std::vector<Range> phi(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateFunction(ip.position(),phi);

        // compute u at integration point
        RF u=0.0;
        for (size_type i=0; i<lfsu.size(); ++i)
          u += x(lfsu,i)*phi[i];

        // evaluate gradient of basis functions on reference element
        std::vector<Jacobian> js(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateJacobian(ip.position(),js);

        // transform gradients from reference element to real element
        const Dune::FieldMatrix<DF,dimw,dim>
          &jac = eg.geometry().jacobianInverseTransposed(ip.position());
        std::vector<Gradient> gradphi(lfsu.size());
        for (size_type i=0; i<lfsu.size(); i++)
          jac.mv(js[i][0],gradphi[i]);

        // compute gradient of u
        Gradient gradu(0.0);
        for (size_type i=0; i<lfsu.size(); ++i)
          gradu.axpy(x(lfsu,i),gradphi[i]);

        // evaluate parameters;
        // Dune::FieldVector<RF,dim>
        //   globalpos = eg.geometry().global(ip.position());
        RF f = 0;

        // get the vector-field beta:
        typename BETA::Traits::RangeType beta;
        Dune::FieldVector<DF,dim> localcenter =
          Dune::ReferenceElements<DF,dim>::general(gt).position(0,0);
        betaF.evaluate( eg.entity(), localcenter, beta );

        // compute meshsize
        RF h = pow(eg.geometry().volume(), 1/DF(dim));

        // Standard Galerkin
        // Diffusion Term:  eps * grad u * grad phi_i
        // Convection Term: - u * beta * grad phi_i
        // Source Term:     - f phi_i
        RF factor = ip.weight()*eg.geometry().integrationElement(ip.position());
        for (size_type i=0; i<lfsu.size(); ++i)
          r.accumulate(lfsu, i, factor * ( epsilon * (gradu*gradphi[i])
                                           - u * (beta*gradphi[i])
                                           - f*phi[i] ));

        if( method == streamlineDiffusion ) {
          // >>> TODO: Begin: Insert your code here (1/1) <<<
          // 1.) calculate deltaSD
          // 2.) add the terms
          // + deltaSD * (beta * grad u) * (beta * grad phi_i)
          // - deltaSD * f * (beta * grad phi_i)

          // 1.) calculate deltaSD here...
          RF deltaSD(0.0);
          RF Peclet = beta.two_norm() * h / 2.0 / epsilon;
          if( Peclet > 1.0 )
            deltaSD = 0.5 * h / beta.two_norm() * ( 1.0 - 1.0 / Peclet );

          // 2.) add the streamline diffusion terms here...
          for (size_type i=0; i<lfsu.size(); ++i)
            r.accumulate(lfsu, i, factor * (
                              deltaSD * (beta * gradu) * (beta * gradphi[i])
                              - deltaSD * f * (beta * gradphi[i]) ));

          // >>> End: Insert (1/1) <<<
        }
      }
  }

  // boundary integral
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s,
                       const LFSV& lfsv_s, R& r_s) const
  {
    // assume Galerkin: lfsu_s == lfsv_s
    // This yields more efficient code since the local functionspace only
    // needs to be evaluated once, but would be incorrect for a finite volume
    // method

    // some types
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType Jacobian;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType Range;
    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = IG::dimension;
    const int dimw = IG::dimensionworld;

    // select quadrature rule for face
    Dune::GeometryType gtface = ig.geometryInInside().type();
    const Dune::QuadratureRule<DF,dim-1>&
      rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

    // get the vector-field beta:
    Dune::FieldVector<DF,dim-1> facecenterlocal =
      Dune::ReferenceElements<DF,dim-1>::general(gtface).position(0,0);
    Dune::FieldVector<DF,dim> facecenterinelement =
      ig.geometryInInside().global( facecenterlocal );

    const auto& eg = ig.inside();

    typename BETA::Traits::RangeType beta;
    betaF.evaluate( eg, facecenterinelement, beta );

    // loop over quadrature points and integrate normal flux
    for (const auto& ip : rule)
      {
        // skip rest if we are on Dirichlet boundary
        if ( bctype.isDirichlet( ig, ip.position() ) )
          continue;

        // position of quadrature point in local coordinates of element
        Dune::FieldVector<DF,dim> local = ig.geometryInInside().global(ip.position());

        // evaluate basis functions at integration point
        std::vector<Range> phi(lfsu_s.size());
        lfsu_s.finiteElement().localBasis().evaluateFunction(local,phi);

        // evaluate gradient of basis functions on reference element
        std::vector<Jacobian> js(lfsu_s.size());
        lfsu_s.finiteElement().localBasis().evaluateJacobian(local,js);

        // transform gradients from reference element to real element
        const Dune::FieldMatrix<DF,dimw,dim>
          jac = eg.geometry().jacobianInverseTransposed(local);
        std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu_s.size());
        for (size_type i=0; i<lfsu_s.size(); i++)
          jac.mv(js[i][0],gradphi[i]);

        // compute gradient of u
        Dune::FieldVector<RF,dim> gradu(0.0);
        for (size_type i=0; i<lfsu_s.size(); i++)
          gradu.axpy(x_s(lfsu_s,i),gradphi[i]);

        // evaluate u (e.g. flux may depend on u)
        RF u=0.0;
        for (size_type i=0; i<lfsu_s.size(); ++i)
          u += x_s(lfsu_s,i)*phi[i];

        RF factor = ip.weight()*ig.geometry().integrationElement(ip.position());
        Dune::FieldVector<DF, dimw> normal = ig.unitOuterNormal(ip.position());
        if ( bctype.isNeumann( ig, ip.position() ) ) {
          // evaluate Neuman boundary condition
          // prescribe total flux j
          // \{ -eps \nabla u + u * beta \} \cdot n = j
          // ==> Integrate j * phi_i
          RF j = 0;
          for (size_type i=0; i<lfsu_s.size(); ++i)
            r_s.accumulate(lfsu_s,i,j*phi[i]*factor);
        }

        if ( bctype.isOutflow( ig, ip.position() ) ) {
          // evaluate outflow boundary condition
          // prescribe diffusive flux j_eps
          //    - eps * \nabla u \cdot n = j_eps
          // ==> Integrate \{ j_eps + u * beta \cdot n \} * phi_i

          // Assume no diffusion on outflow boundary
          RF j_epsilon = 0;

          RF term = factor * ( j_epsilon + u * (beta * normal) );
          for (size_type i=0; i<lfsu_s.size(); ++i)
            r_s.accumulate(lfsu_s, i, term * phi[i]);
        }
      }
  }

private:
  const BCType& bctype;
  const BETA& betaF;
  Eps epsilon;
  Method method;
  unsigned int intorder;
};

// set characteristic length of elements
Mesh.CharacteristicLengthMax = 1.0/32;

// points are best entered manually
Point(1) = {0, 0, 0, 1e22};

Extrude {1,0,0} {
  Point{1};
}
Extrude {0,1,0} {
  Line{1};
}

radius = 0.45;
Point(11) = {0.5-radius, 0.5, 0, 1e22};
Point(12) = {0.5+radius, 0.5, 0, 1e22};
Point(13) = {0.5, 0.5-radius, 0, 1e22};
Point(14) = {0.5, 0.5+radius, 0, 1e22};

Extrude {{0,0,1}, {.5,.5,0}, Pi/2} {
  Point{11,14,12,13};
}
Delete {
  Surface{5};
}

Line Loop(10) = {1,4,-2,-3};
Line Loop(11) = {9,8,7,6};
Plane Surface(12) = {10,11};

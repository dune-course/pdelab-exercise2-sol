template<typename GV, typename RF>
class BETA
  : public Dune::PDELab::GridFunctionBase<
           Dune::PDELab::GridFunctionTraits<GV,RF,GV::dimension,
                                            Dune::FieldVector<RF,GV::dimension> >,
           BETA<GV,RF> >
{
  const GV& gv;

public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,
      GV::dimension, Dune::FieldVector<RF,GV::dimension> > Traits;
  typedef Dune::PDELab::GridFunctionBase<
          Dune::PDELab::GridFunctionTraits<GV, RF,GV::dimension,
                                           Dune::FieldVector<RF,GV::dimension> >,
          BETA<GV,RF> > BaseT;

  BETA (const GV& gv_) : gv(gv_) {}

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    y = 0;

    y[0] = 1.0;
    y[1] = 0.25;

    // An example for another velocity field maybe...:
    // typename Traits::DomainType xg = e.geometry().global(x);
    // y[0] = 1.0;
    // y[1] = 0.5 * cos(10*xg[0]) ;
  }

  inline const typename Traits::GridViewType& getGridView () { return gv; }
};



template<typename Real, typename U, typename GV, typename GFS,
         typename CC, typename BCTypeParam, typename BETA, typename Eps>
void sdsolver( U& u, const GV& gv, const GFS& gfs, const CC& cc,
               const BCTypeParam& bctype, const BETA& beta, Eps epsilon,
	       Method method, bool damping, const std::string &methodname,
               const std::string outputname)
{
  // <<<3>>> Make grid operator space
  typedef Example02bLocalOperator<BCTypeParam,BETA,Eps> LOP;    // operator including boundary
  LOP lop( bctype, beta, epsilon, method );
  typedef Dune::PDELab::istl::BCRSMatrixBackend<std::size_t> MBE;

  typedef Dune::PDELab::GridOperator<
    GFS,GFS,        /* ansatz and test space */
    LOP,            /* local operator */
    MBE,            /* matrix backend */
    Real,Real,Real, /* field types for domain, range and jacobian */
    CC,CC           /* constraints transformation  for ansatz and test space */
    > GO;
  GO go(gfs,cc,gfs,cc,lop,MBE(9));

  // <<<5>>> Select a linear solver backend
  // typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  // LS ls(5000,true);
  typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LS;
  LS ls(true);

  // <<<6>>> assemble and solve linear problem
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  SLP slp(go,ls,1e-10);
  slp.apply(u);

  // <<<7>>> graphical output
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  DGF udgf(gfs,u);
  Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTK::conforming);
  typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKADAPTER;
  vtkwriter.addVertexData(std::make_shared<VTKADAPTER>(udgf,"solution"));
  vtkwriter.write(outputname,Dune::VTK::appendedraw);
  std::cout << "Output of 2b: " << methodname << " --> "
            << outputname << ".vtu" << std::endl << std::endl;
}

template<class GV, class VEC, class Eps>
void example02b_Q1 (const GV& gv, const VEC& dx, Eps epsilon)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,1> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::ConformingDirichletConstraints CONSTRAINTS; // constraints class
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CONSTRAINTS,VBE> GFS;
  GFS gfs(gv,fem);

  typedef BETA<GV,Real> BETAType;
  BETAType beta(gv);
  BCTypeParam<BETAType> bctype(beta); // boundary condition type
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  Dune::PDELab::constraints( bctype, gfs, cc ); // assemble constraints
  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

  // <<<3>>> moved into sdsolver()

  // <<<4>>> Make FE function extending Dirichlet boundary conditions
  // BackendVectorSelector can be used if a vector is needed but no
  // GridOperator is available yet
  typedef typename Dune::PDELab::BackendVectorSelector<GFS,Real>::Type U;
  U u(gfs,0.0);
  typedef BCExtension_2b<GV,Real,VEC> G;                        // boundary value + extension
  G g(gv,dx);
  Dune::PDELab::interpolate(g,gfs,u);                           // interpolate coefficient vector

  // <<<5>>> - <<<7>>> moved inside "sdsolver"

  sdsolver<Real>( u,gv,gfs,cc,bctype,beta,epsilon, standardGalerkin, false,
                  "Standard Galerkin", "example02b_SG");

  // Reset solution vector to start from a clean slate
  u = 0;
  Dune::PDELab::interpolate(g,gfs,u);
  sdsolver<Real>( u,gv,gfs,cc,bctype,beta,epsilon, streamlineDiffusion, false,
                  "Streamline Diffusion", "example02b_SD");
}
